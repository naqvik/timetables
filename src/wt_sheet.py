import xlwt
def create_timetables(term, timetables_data, output_file):
    '''Create a timetable spreadsheet with 1 tab per timetable.

    The first input parameter is the term, a 6-digit field which holds
    the year and semester (10,20, or 30).  Example: '201430' is 2014 Fall.
    
    The second input parameter is a list of 2-tuples consisting of
    (tag,dictionary).  Each tag identifies the program, semester, and
    optionally the subgroup of a particular timetable. Each dictionary
    element contains the data to create that timetable.  Example: [
    (tag1,dict1), (tag2, dict2), ...]

    Each tag has the form 'PSE-8-A', 'ESE-3', 'COMMON-1-A', etc.

    A single dictionary will have keys corresponding to weekdays: 'M',
    'T', 'W', 'R', and 'F'.  The associated values are lists of
    timeslot tuples, in increasing order of begin time.  Thus
    iterating through the days of the week, and the timeslots for each
    day, produces every timeslot, in start order, throughout the week.

    Example of dictionary structure, for the ith member:

    dict_i = {
     'M': [ timeslot1, timeslot2, ...], 'T': [timeslot1, timeslot2, ...],
     'W': [ timeslot1, timeslot2, ...], 'R': [timeslot1, timeslot2, ...],
     'F': [timeslot1, timeslot2, ...],}

    '''
    timetables = xlwt.Workbook()
    # sheet_names = [("First_year_sem1_p1", "FIRST YEAR - SEMESTER 1",),
    #                #("First_year_sem1_p2", "FIRST YEAR - SEMESTER 1",),
    #                ("ese_sem3", u"ESE - SEMESTER 3",),
    #                ("ese_sem5", u"ESE - SEMESTER 5",),
    #                ("ese_sem8", u"ESE - SEMESTER 8",),
    #                ("evse_sem3",u"EVSE - SEMESTER 3",),
    #                ("evse_sem5",u"EVSE - SEMESTER 5",),
    #                ("evse_sem8",u"EVSE - SEMESTER 8",),
    #                ("ise_sem3", u"ISE - SEMESTER 3",),
    #                ("ise_sem5", u"ISE - SEMESTER 5",),
    #                ("ise_sem8", u"ISE - SEMESTER 8",),
    #                ("pse_sem3", u"PSE - SEMESTER 3",),
    #                ("pse_sem5", u"PSE - SEMESTER 5",),
    #                ("pse_sem8", u"PSE - SEMESTER 8",),
    #                ("sse_sem3", u"SSE - SEMESTER 3",),
    #                ("sse_sem5", u"SSE - SEMESTER 5",),
    #                ("sse_sem8", u"SSE - SEMESTER 8",),
    #            ]

    for ((tag, dict_i)) in timetables_data:
        # can't have hyphens in an excel TAB name
        sheet = timetables.add_sheet(tag.replace('-', '_'))
        _fill_one_sheet(sheet, term, tag, dict_i)

    timetables.save(output_file)

def _fill_one_sheet(sheet, term, tag, timetable_data):
    title_style = xlwt.easyxf('font: bold true; align: horizontal centre')
    sheet.write_merge(0,0,0,5, "SUBJECT TO CHANGE", title_style)

    # FIXME: how is this date info passed to this routine?
    sheet.write(1,5, term, title_style)

    title_style = xlwt.easyxf('font: bold true; align: horizontal left')

    (name, sem) = tag.split('-')[0:2]
    sheet_title = "%s - SEMESTER %s (%s)" % (name, sem, tag)
    sheet.write_merge(1,1,0,4, sheet_title, title_style)

    # print day (col) headers, and set column widths
    for i,header in enumerate(['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY']):
        sheet.col(i+1).width = 256*15
        sheet.row(2).write(i+1, header, xlwt.easyxf(
            'border: left medium, right medium, top medium, bottom medium;'
            'align: horizontal centre, vertical centre'))

    # print time (row) labels, and set row properties
    #row_style = xlwt.easyxf( 'font: height 800;' '')
    for i in range(12):
        hour = i+8
        #sheet.write(2*i+3, 0, '%02d:30'%hour)
        time_style = xlwt.easyxf('align: horiz centre, vert top;'
        'border: left medium, right medium, top medium, bottom medium;')
        sheet.write_merge(2*i+3,2*i+4,0,0, '%02d:30'%hour, time_style)

        sheet.row(2*i+3).height_mismatch = 1
        sheet.row(2*i+3).height = 500
        sheet.row(2*i+4).height_mismatch = 1
        sheet.row(2*i+4).height = 500
        #sheet.row(2*i+3).set_style(row_style)
        #sheet.row(2*i+3).height = 25

    # fill columns for mon, tue, ..., fri
    for (i, day) in enumerate(('M','T','W','R','F',)):
        col = i+1
        row = 3
        _fill_column(sheet, row, col, timetable_data[day])
    
def _fill_column(sheet, start_row, column, timeslot_list):
    coalesced_list = coalesce_timeslots(timeslot_list)
    for i,t in enumerate( coalesced_list ):
        text = stringize_timeslots(t)
        next_start_time = '2330' # effectively infinite
        if i+1 < len(coalesced_list):
            next_start_time = coalesced_list[i+1][0][4]
        try:
            (offset, end_row) = timeslot_span(t, next_start_time)
        except ValueError as ve:
            print ve.message
            print 'Skipping...'
            continue
            
        row1 = start_row + offset
        row2 = start_row + end_row -1
        style = xlwt.easyxf('align: horiz centre, vert top;'
                            'font: height 140;'
                            'border: left thin, right thin, top thin, bottom thin;')
        sheet.write_merge(row1,row2,column,column,text,style)
        #sheet.write(row1,column,text,style)

def half_hour_ceil(end_time_str):
    '''Round the time string up to the nearest multiple of 30 minutes'''
    assert '0830' < end_time_str < '2300', 'Unexpected time %s.' %end_time_str
    hour = end_time_str[0:2]
    minute = end_time_str[2:4]
    if minute == '00':
        return end_time_str
    elif '00' < minute <= '30':
        end_time_str = end_time_str[0:2] + '30'
    else:
        end_time_str = "%.02s00" % (int(hour) + 1,)
    return end_time_str


def timeslot_span(timeslot_tuple, clip_time):
    '''First argument is a tuple of coalesced timeslots, e.g.:
      ( timeslot1, timeslot2, ...)
    At least 1 timeslot will be present.

    2nd argument is a 4-digit 24h time at which the next timeslot block begins.
    '''
    all_start_times = (
        '0830', '0900', '0930', '1000', '1030', '1100', '1130', '1200', '1230',
        '1300', '1330', '1400', '1430', '1500', '1530', '1600', '1630', '1700',
        '1730', '1800', '1830', '1900', '1930', '2000', '2030', '2100', '2130',
        '2200', '2230', '2300')
    start_time = timeslot_tuple[0][4]
    #print 'size: %s, 1st tuple:%s' % (len(timeslot_tuple), timeslot_tuple[0])
    #print 'start_time:', start_time
    try:
        row_offset = all_start_times.index(start_time)
    except ValueError:
        message = 'Unacceptable start time: ' +  start_time + \
                  ' for course: %s %s %s' % timeslot_tuple[0][0:3]
        raise(ValueError(message))
    max_end_time = half_hour_ceil( max( [ts[5] for ts in timeslot_tuple] ) )
    max_end_time = min( max_end_time, clip_time ) # don't overlap next block
    #print 'max_end_time:', max_end_time
    end_row = all_start_times.index(max_end_time)
    #print  (row_offset, end_row)
    return  (row_offset, end_row)


def stringize_timeslots(timeslot_tuple):
    text = ["%s %s %s\n%.3s %s-%s" % ts[0:-1] for ts in timeslot_tuple]
    return '\n'.join(text)

def coalesce_timeslots(timeslot_list):
    def same_time(ts1, ts2):
        return ts1[4] == ts2[4] # same start time

    coalesced_list = []
    length = len(timeslot_list)
    if length == 0:
        return coalesced_list

    i = 0
    while i != length:
        ts = timeslot_list[i]
        j = i + 1
        while (j != length and same_time(ts, timeslot_list[j])):
            j += 1
        coalesced_list.append( tuple(timeslot_list[i:j]) )
        i = j
                               
    return coalesced_list
            
