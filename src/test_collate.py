'''Test functionality of collation function, which is named group_course_sections_by_day.
'''
from rd_sheet import create_empty_per_day_dict
from rd_sheet import group_course_sections_by_day

input_timeslots = [
    ('ENEL', '280', '001',  'Lecture', '1530', '1620', ('M',  'W',  'F')),
    ('ENEL', '280', '081',  'Seminar', '0830', '1115',  ('T',)   ),
    ('ENEL', '280', '082',  'Seminar', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '091',  'Lab', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '092',  'Lab', '0830', '1115', ('M', )   ),
    ('ENEL', '280', '093',  'Lab', '0830', '1115',     ('F',)),
    ('ENEL', '280', '094',  'Lab', '1430', '1715',    ('R',) ),
    ('ENEL', '280', '095',  'Lab', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '096',  'Lab', '1130', '1415',     ('F',)),
    ('ENEL', '280', '097',  'Lab', '1130', '1415',   ('W',)  ),
    ('ENEL', '280', '098',  'Lab', '1430', '1715',    ('R',) ),
    ]

expected = {
    'M': [
        ('ENEL', '280', '001',  'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '092',  'Lab', '0830', '1115', ('M', )   ),
    ],
    'T': [
        ('ENEL', '280', '081',  'Seminar', '0830', '1115',  ('T',)   ),
        ('ENEL', '280', '082',  'Seminar', '1430', '1715',  ('T',)   ),
        ('ENEL', '280', '091',  'Lab', '1430', '1715',  ('T',)   ),
        ('ENEL', '280', '095',  'Lab', '1430', '1715',  ('T',)   ),
    ],
    'W': [
        ('ENEL', '280', '001',  'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '097',  'Lab', '1130', '1415',   ('W',)  ),
    ],

    'R': [
        ('ENEL', '280', '094',  'Lab', '1430', '1715',    ('R',) ),
        ('ENEL', '280', '098',  'Lab', '1430', '1715',    ('R',) ),
    ],
    'F': [
        ('ENEL', '280', '001',  'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '093',  'Lab', '0830', '1115',     ('F',)),
        ('ENEL', '280', '096',  'Lab', '1130', '1415',     ('F',)),
    ],
}     

per_day_dict = create_empty_per_day_dict()
per_day_dict = group_course_sections_by_day(input_timeslots, per_day_dict)

for d in ('M', 'T', 'W', 'R', 'F'):
    #print d
    assert len(per_day_dict[d]) == len(expected[d]), 'lengths differ: actual %s, expected %s' % (per_day_dict[d], expected[d])
    for a,e in zip(per_day_dict[d],expected[d]):
        #print 'a=', a
        #print 'e=', e
        if a != e:
            print 'error'
            print '     got:', a
            print 'expected:', e
            assert 0
        else:
            pass

