'''Test the code that returns a non-sorted list of timeslot tuples,
given a course name/number pair like 'ENEL' '280'.

'''
from rd_sheet import matching_course_sections
from rd_sheet import read_schedule_sheet

sheet = read_schedule_sheet("../spreadsheets/201430 - all classes.xlsx")
timeslots = matching_course_sections(sheet, ('ENEL', '280'))
expected = [
    ('ENEL', '280', '001',  'Lecture', '1530', '1620', ('M',  'W',  'F')),
    ('ENEL', '280', '081',  'Seminar', '0830', '1115',  ('T',)   ),
    ('ENEL', '280', '082',  'Seminar', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '091',  'Lab', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '092',  'Lab', '0830', '1115', ('M', )   ),
    ('ENEL', '280', '093',  'Lab', '0830', '1115',     ('F',)),
    ('ENEL', '280', '094',  'Lab', '1430', '1715',    ('R',) ),
    ('ENEL', '280', '095',  'Lab', '1430', '1715',  ('T',)   ),
    ('ENEL', '280', '096',  'Lab', '1130', '1415',     ('F',)),
    ('ENEL', '280', '097',  'Lab', '1130', '1415',   ('W',)  ),
    ('ENEL', '280', '098',  'Lab', '1430', '1715',    ('R',) ),
    ]

 
for (a,e) in zip(timeslots, expected):
    if a != e:
        print 'error:'
        print '     got:', a
        print 'expected:', e
# if timeslots != expected:
#     print 'error: got',
#     for t in timeslots:
#         print t
