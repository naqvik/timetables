'''Run some tests using the unittest framework.
FIXME: the name of this file is too specific.  There are unrelated
tests run by this file.

'''

import unittest
from rd_sheet import read_schedule_sheet
from rd_sheet import get_one_semester
from rd_sheet import create_per_day_dict
from rd_sheet import sort_all_days

class Test_rd_sheet(unittest.TestCase):
    def setUp(self):
        self.schedule_sheet = read_schedule_sheet("../spreadsheets/201430 - all classes.xlsx")
        self.all_pgms = {
            "COMMON-1-A": [("CHEM", "104"), ("ENGG", "123"), ("MATH", "122"),
                           ("PHYS", "109"), ("MATH", "110"),],
            "COMMON-1-B": [("ENGL", "100"), ("ENGG", "100"), ("MATH", "122"),
                           ("PHYS", "109"), ("MATH", "110"),],
            "ESE-3": [("MATH", "217"), ("CS", "115"), ("ENEL", "280"),
                      ("ENEV", "223"), ("ENGG", "240"),],
            "ESE-5": [("ENSE", "352"), ("ENEL", "383"), ("BUS", "260"),
                      ("ENEL", "384"), ("PHYS", "201"),],
        }

    def test_get_one_semester_2014_ese_sem5(self):
        courses = self.all_pgms['ESE-5']
        expected = [('ENSE', '352'), ('ENEL', '383'), ('BUS', '260'),
                    ('ENEL', '384'), ('PHYS', '201')]
        self.assertEqual(courses, expected)

    def test_create_per_day_dict(self):
        courses = ( ('ENSE', '352',), )
        per_day_dict = create_per_day_dict('ESE-3', self.schedule_sheet, courses)
        
        expected = {
            'M': [
                ('ENSE', '352', '091', 'Lab', '1430', '1715', ('M',)),
            ],
            'T': [
                ('ENSE', '352', '001', 'Lecture', '1430', '1520', ('T', 'R', 'F')),
            ],
            'W': [
                ('ENSE', '352', '093', 'Lab', '1430', '1715', ('W',)),
            ],
            'R': [
                ('ENSE', '352', '001', 'Lecture', '1430', '1520', ('T', 'R', 'F')),
            ],
            'F': [
                ('ENSE', '352', '001', 'Lecture', '1430', '1520', ('T', 'R', 'F')),
                ('ENSE', '352', '092', 'Lab', '1130', '1415', ('F',)),
            ],}

        for d in ['M','T','W','R','F']:
            self.assertEqual(per_day_dict[d], expected[d])

    # def test_2014_common_sem1(self):
    #     self.assertTrue(0)

    def test_2014_ese_sem3(self):
        expected_ese_sem3 = {
            'R': [('ENGG', '240', '091', 'Lab', '0830', '1115', ('R',)),
                  ('ENGG', '240', '002', 'Lecture', '1130', '1220', ('T', 'R', 'F')),
                  ('MATH', '217', '001', 'Lecture', '1330', '1420', ('T', 'R', 'F')),
                  ('ENEL', '280', '094', 'Lab', '1430', '1715', ('R',)),
                  ('ENEL', '280', '098', 'Lab', '1430', '1715', ('R',)),
                  ('CS', '115', '095', 'Lab', '1530', '1720', ('R',))],
            'M': [('ENEL', '280', '092', 'Lab', '0830', '1115', ('M',)),
                  ('ENEV', '223', '001', 'Lecture', '0830', '0920', ('M', 'W', 'F')),
                  ('CS', '115', '091', 'Lab', '1030', '1220', ('M',)),
                  ('CS', '115', '001', 'Lecture', '1300', '1415', ('M', 'W')),
                  ('ENEL', '280', '001', 'Lecture', '1530', '1620', ('M', 'W', 'F')),
                  ('ENGG', '240', '001', 'Lecture', '1630', '1720', ('M', 'W', 'F'))],
            'T': [('ENEL', '280', '081', 'Seminar', '0830', '1115', ('T',)),
                  ('CS', '115', '092', 'Lab', '1030', '1220', ('T',)),
                  ('ENGG', '240', '002', 'Lecture', '1130', '1220', ('T', 'R', 'F')),
                  ('MATH', '217', '001', 'Lecture', '1330', '1420', ('T', 'R', 'F')),
                  ('ENEL', '280', '082', 'Seminar', '1430', '1715', ('T',)),
                  ('ENEL', '280', '091', 'Lab', '1430', '1715', ('T',)),
                  ('ENEL', '280', '095', 'Lab', '1430', '1715', ('T',)),
                  ('ENEV', '223', '091', 'Lab', '1730', '1820', ('T',))],
            'W': [('ENEV', '223', '001', 'Lecture', '0830', '0920', ('M', 'W', 'F')),
                  ('CS', '115', '094', 'Lab', '1030', '1220', ('W',)),
                  ('ENEL', '280', '097', 'Lab', '1130', '1415', ('W',)),
                  ('CS', '115', '001', 'Lecture', '1300', '1415', ('M', 'W')),
                  ('ENEL', '280', '001', 'Lecture', '1530', '1620', ('M', 'W', 'F')),
                  ('ENGG', '240', '001', 'Lecture', '1630', '1720', ('M', 'W', 'F'))],
            'F': [('ENEL', '280', '093', 'Lab', '0830', '1115', ('F',)),
                  ('ENEV', '223', '001', 'Lecture', '0830', '0920', ('M', 'W', 'F')),
                  ('CS', '115', '093', 'Lab', '1030', '1220', ('F',)),
                  ('ENEL', '280', '096', 'Lab', '1130', '1415', ('F',)),
                  ('ENGG', '240', '002', 'Lecture', '1130', '1220', ('T', 'R', 'F')),
                  ('MATH', '217', '001', 'Lecture', '1330', '1420', ('T', 'R', 'F')),
                  ('ENEL', '280', '001', 'Lecture', '1530', '1620', ('M', 'W', 'F')),
                  ('ENGG', '240', '001', 'Lecture', '1630', '1720', ('M', 'W', 'F'))]}

        courses = self.all_pgms['ESE-3']

        # self.assertEqual(courses, ('MATH 217', 'CS 115', 'ENEL 280', 'ENEV 223', 'ENGG 240'))
        per_day_dict = create_per_day_dict('ESE-3', self.schedule_sheet, courses)
        sort_all_days(per_day_dict)
        for d in ['M', 'T', 'W', 'R', 'F']:
            for a,e  in zip(per_day_dict[d], expected_ese_sem3[d]):
                self.assertEqual(a,e)
        self.assertEqual(per_day_dict, expected_ese_sem3)

if __name__ == "__main__":
    unittest.main()
