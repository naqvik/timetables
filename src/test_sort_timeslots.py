'''Test the sorting of timeslot tuples, by begin_time.  The following
test vectors are extracted from the spreadsheet for 2014 fall,
selecting only the courses for ESE semester 3, which are: cs115,
math217, enel280, engg240, and enev223.

The result vectors show what the output after sorting is expected to be.
'''

from rd_sheet import sort_timeslots

if __name__ == "__main__":
    monday_vec = [
        ('CS', '115', '001', 	'Lecture', '1300', '1415', ('M',  'W',)  ),
        ('CS', '115', '091', 	'Lab', '1030', '1220', ('M',)    ),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '092', 	'Lab', '0830', '1115', ('M', )   ),
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
        ]

    tuesday_vec = [
        ('MATH', '217', '001', 	'Lecture', '1330', '1420',  ('T',  'R', 'F')),
        ('CS', '115', '092', 	'Lab', '1030', '1220',  ('T',)   ),
        ('ENEL', '280', '081', 	'Seminar', '0830', '1115',  ('T',)   ),
        ('ENEL', '280', '082', 	'Seminar', '1430', '1715',  ('T',)   ),
        ('ENEL', '280', '091', 	'Lab', '1430', '1715',  ('T',)   ),
        ('ENEL', '280', '095', 	'Lab', '1430', '1715',  ('T',)   ),
        ('ENEV', '223', '091', 	'Lab', '1730', '1820',  ('T',)   ),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
        ]

    wednesday_vec = [
        ('CS', '115', '001', 	'Lecture', '1300', '1415', ('M',  'W',)  ),
        ('CS', '115', '094', 	'Lab', '1030', '1220',   ('W',)  ),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '097', 	'Lab', '1130', '1415',   ('W',)  ),
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
    ]


    thursday_vec = [
        ('MATH', '217', '001', 	'Lecture', '1330', '1420',  ('T',  'R', 'F')),
        ('CS', '115', '095', 	'Lab', '1530', '1720',    ('R',) ),
        ('ENEL', '280', '094', 	'Lab', '1430', '1715',    ('R',) ),
        ('ENEL', '280', '098', 	'Lab', '1430', '1715',    ('R',) ),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
        ('ENGG', '240', '091', 	'Lab', '0830', '1115',    ('R',) ),
    ]

    friday_vec = [
        ('MATH', '217', '001', 	'Lecture', '1330', '1420',  ('T',  'R', 'F')),
        ('CS', '115', '093', 	'Lab', '1030', '1220',     ('F',)),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENEL', '280', '093', 	'Lab', '0830', '1115',     ('F',)),
        ('ENEL', '280', '096', 	'Lab', '1130', '1415',     ('F',)),
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
    ]

    mon_expected = [
        ('ENEL', '280', '092', 	'Lab', '0830', '1115', ('M', )   ),
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('CS', '115', '091', 	'Lab', '1030', '1220', ('M',)    ),
        ('CS', '115', '001', 	'Lecture', '1300', '1415', ('M',  'W',)  ),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
        ]

    tue_expected = [
        ('ENEL', '280', '081', 'Seminar', '0830', '1115', ('T',),),
        ('CS', '115', '092', 	'Lab', '1030', '1220', ('T',), ),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
        ( 'MATH', '217', '001', 'Lecture', '1330', '1420', ('T',  'R', 'F')),
        ('ENEL', '280', '082', 'Seminar', '1430', '1715', ('T',)),
        ('ENEL', '280', '091', 	'Lab', '1430', '1715',  ('T',)   ),
        ('ENEL', '280', '095', 	'Lab', '1430', '1715',  ('T',)   ),
        ('ENEV', '223', '091', 	'Lab', '1730', '1820',  ('T',)   ),
    ]
    wed_expected = [
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('CS', '115', '094', 	'Lab', '1030', '1220',   ('W',)  ),
        ('ENEL', '280', '097', 	'Lab', '1130', '1415',   ('W',)  ),
        ('CS', '115', '001', 	'Lecture', '1300', '1415', ('M',  'W',)  ),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
    ]

    thu_expected = [
        ('ENGG', '240', '091', 	'Lab', '0830', '1115',    ('R',) ),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
        ('MATH', '217', '001', 	'Lecture', '1330', '1420',  ('T',  'R', 'F')),
        ('ENEL', '280', '094', 	'Lab', '1430', '1715',    ('R',) ),
        ('ENEL', '280', '098', 	'Lab', '1430', '1715',    ('R',) ),
        ('CS', '115', '095', 	'Lab', '1530', '1720',    ('R',) ),
    ]

    fri_expected = [
        ('ENEL', '280', '093', 	'Lab', '0830', '1115',     ('F',)),
        ('ENEV', '223', '001', 	'Lecture', '0830', '0920', ('M',  'W',  'F')),
        ('CS', '115', '093', 	'Lab', '1030', '1220',     ('F',)),
        ('ENEL', '280', '096', 	'Lab', '1130', '1415',     ('F',)),
        ('ENGG', '240', '002', 	'Lecture', '1130', '1220',  ('T',  'R', 'F')),
        ('MATH', '217', '001', 	'Lecture', '1330', '1420',  ('T',  'R', 'F')),
        ('ENEL', '280', '001', 	'Lecture', '1530', '1620', ('M',  'W',  'F')),
        ('ENGG', '240', '001', 	'Lecture', '1630', '1720', ('M',  'W',  'F')),
    ]


    def check_one_day(day_vec, day_expected):
        actual = sort_timeslots(day_vec)
        assert len(actual) == len(day_vec)
        for actblk, expectblk in zip(actual,day_expected):
            if actblk != expectblk:
                print "error: \nexpected %s\n     got %s" % (expectblk, actblk)
                assert(0)
        
    check_one_day(monday_vec, mon_expected)
    check_one_day(tuesday_vec, tue_expected)
    check_one_day(wednesday_vec, wed_expected)
    check_one_day(thursday_vec, thu_expected)
    check_one_day(friday_vec, fri_expected)

