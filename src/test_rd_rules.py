'''Unit tests for rd_rules.py'''

import itertools
import unittest
from rd_rules import read_rules, CourseDisplayRule, ValidRuleException


def test_reading_rules_sheet():
    """Read the actual spreadsheet, and compare actual TimeTables with
    expected

    """
    timetables = read_rules('201430', '../spreadsheets/test-rules.xlsx')

    expected = {
        'COMMON-1-A': [
            CourseDisplayRule(
                ['201430', 'COMMON-1-A', 'CHEM', '104', '001,002',
                 '086-097,099', '', ''] ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-A', 'MATH', '110', '003', '030',
                 '', '']
            ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-A', 'ENGG', '123', '001', '091',
                 '',''] ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-A', 'MATH', '122', '002', '',
                 '', ''] ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-A', 'PHYS', '109', '001,003',
                 '121,123,131,133,141,143,151,153','',''] ),
        ],
        'COMMON-1-B': [
            CourseDisplayRule(
                ['201430', 'COMMON-1-B', 'MATH', '110', '001', '010','',''], ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-B', 'ENGG', '100', '001', '091-093',
                 '',''], ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-B', 'MATH', '122', '003', '','',''], ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-B', 'PHYS', '109', '003', '141,143',
                 '',''],
            ),
            CourseDisplayRule(
                ['201430', 'COMMON-1-B', 'ENGL', '100', '001-023', '',
                 '', 'Choose an open section that works.'], ),
        ],
        'ESE-3': [
            CourseDisplayRule(
                ['201430', 'ESE-3', 'ENEL', '280', '001', '095,098',
                 '081,082', ''], ),
            CourseDisplayRule(
                ['201430', 'ESE-3', 'ENGG', '240', '001', '091', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-3', 'MATH', '217', '001', '','',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-3', 'CS', '115', '001', '091-095', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-3', 'ENEV', '223', '001', '091', '',''], ),
        ],
        'ESE-5': [
            CourseDisplayRule(
                ['201430', 'ESE-5', 'BUS', '260', '001,003', '','',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-5', 'ENEL', '383', '001', '091-093', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-5', 'ENEL', '384', '001', '091-093', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-5', 'ENSE', '352', '001', '091-093', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-5', 'PHYS', '201', '001', '121,141', '',''], ),
        ],
        'ESE-8': [
            CourseDisplayRule(
                ['201430', 'ESE-8', 'ENEL', '389', '001', '091,092', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-8', 'ENEL', '393', '001', '091,092', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-8', 'ENEL', '400', '001', '091', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-8', 'ENEL', '472', '001', '091,092', '',''], ),
            CourseDisplayRule(
                ['201430', 'ESE-8', 'ENGG', '303', '001', '', '081,082', ''], ),
        ],

    }
    assert len(expected) == len(timetables), \
        "Expected %s timetables, got %s\n" % (len(expected), len(timetables))
    for tag in sorted(expected.keys()):
        for (expect, actual) in itertools.izip_longest(
                expected[tag], timetables.timetable(tag).get_courses()):
            assert expect == actual, \
                "For tag %s, expected %s, got %s" % (tag, expect, actual)

from rd_rules import valid_seq_numb
class TestValidSeqNumber(unittest.TestCase):
    def test_001(self):
        n = valid_seq_numb('001')
        self.assertEqual(n, set([1]))

    def test_086_to_097_099(self):
        s = valid_seq_numb('086-097,099')
        self.assertEqual(s, set(range(86,98) + [99]))

    def test_001_002(self):
        s = valid_seq_numb('001,002')
        self.assertEqual(s, set([1,2]))

    def test_121_141(self):
        s = valid_seq_numb('121,141')
        self.assertEqual(s, set([121,141]))
    def test_191_to_193(self):
        s = valid_seq_numb('091-093')
        self.assertEqual(s, set([91,92,93]))

    def test_001_to_023(self):
        s = valid_seq_numb('001-023')
        self.assertEqual(s, set(range(1,24)))

    def test_irregularly_spaced(self):
        s = valid_seq_numb('121,123,131,133,141,143,151,153')
        self.assertEqual(s, set([121,123,131,133,141,143,151,153]))
    
    def test_1234(self):
        #s = valid_seq_numb('1234')
        self.assertRaises(ValidRuleException, valid_seq_numb, '1234')
    def test_123_to_100(self):
        s = valid_seq_numb('123-100')  # returns an empty list
        self.assertEqual(s, set())

    def test_100_to_2x0(self):
        #s = valid_seq_numb('100-2x0')  # illegal number in range
        self.assertRaisesRegexp(ValidRuleException, '100-2x0', valid_seq_numb, '100-2x0')

    def test_empty_string(self):
        s = valid_seq_numb('')
        self.assertEqual(s, set())

from rd_rules import valid_tag, valid_subj_code, valid_crse_numb

class TestValidRule(unittest.TestCase):
    def test_bad_tag_ESE_0_A(self):
        self.assertRaisesRegexp(
            ValidRuleException, r"encountered: 'ESE-0-A'\n",
            valid_tag, 'ESE-0-A')
    def test_blank_subj_code_non_blank_fields(self):
        self.assertRaisesRegexp(
            ValidRuleException, r'CRSE_NUMB, LECTURES', valid_subj_code,
            ['201430', 'ESE-8', '', '', '001', '', '081,082', 'comment'],
            '')

    def test_malformed_subj_code_MATHS(self):
        self.assertRaisesRegexp(
            ValidRuleException, r"MATHS'$", valid_subj_code,
            [], 'MATHS')

    def test_malformed_crse_numb_23FF(self):
        self.assertRaisesRegexp(
            ValidRuleException, r"'23FF'$", valid_crse_numb, '23FF')

class Test_read_rules(unittest.TestCase):
    def test_non_existent_file(self):
        #tts = read_rules('201430', 'no_such_file.xlsx')
        self.assertRaises(IOError, read_rules, '201430', 'no_such_file.xlsx')

    def test_malformed_subj_code(self):
        self.assertRaisesRegexp(
            ValidRuleException, r"CHEMY", read_rules, '201430',
            '../spreadsheets/test-rules-malformed-subj-code.xlsx')

    def test_malformed_field_names(self):
        self.assertRaises(
            Exception, read_rules, '201430',
            '../spreadsheets/test-rules-malformed-field-names.xlsx')

if __name__ == "__main__":
    test_reading_rules_sheet()
    unittest.main()
