'''Reject and accept rules, which determine whether and how a particular
course offering should be displayed in a timetable.

'''
# Format of an accept rule
#  (tag, subj_code, crse_numb, seq_numb_re, comment)

accept_rule = [
    ('COMMON-1-A', 'CHEM', '104', '00[12]|08[6-9]|09[0-7]|099', ''),
    ('COMMON-1-A', 'MATH', '110', '003|030', ''),
    ('COMMON-1-A', 'ENGG', '123', '001|091', ''),
    ('COMMON-1-A', 'MATH', '122', '002', ''),
    ('COMMON-1-A', 'PHYS', '109', '00[13]|1[2-5][13]', ''),

    ('COMMON-1-B', 'MATH', '110', '001|010', ''),
    ('COMMON-1-B', 'ENGG', '100', '001|09[1-3]', ''),
    ('COMMON-1-B', 'MATH', '122', '003', ''),
    ('COMMON-1-B', 'PHYS', '109', '003|14[13]', ''),
    ('COMMON-1-B', 'ENGL', '100', '00[1-9]', 'Choose an open section that works.'),

    ('ESE-3', 'ENEL', '280', '001|09[58]|08[12]', ''),
    ('ESE-3', 'ENGG', '240', '001|091', ''),
    ('ESE-3', 'MATH', '217', '001', ''),
    ('ESE-3', 'CS'  , '115', '001|09[15]', ''),
    ('ESE-3', 'ENEV', '223', '001|091', ''),

    ('ESE-5', 'BUS', '260', '00[13]', ''),
    ('ESE-5', 'ENEL', '383', '001|09[1-3]', ''),
    ('ESE-5', 'ENEL', '384', '001|09[1-3]', ''),
    ('ESE-5', 'ENSE', '352', '001|09[1-3]', ''),
    ('ESE-5', 'PHYS', '201', '001|1[24]1', ''),

    ('ESE-8', 'ENEL', '389', '001|09[12]', ''),
    ('ESE-8', 'ENEL', '393', '001|09[12]', ''),
    ('ESE-8', 'ENEL', '400', '001|091', ''),
    ('ESE-8', 'ENEL', '472', '001|09[12]', ''),
    ('ESE-8', 'ENGG', '303', '001|08[12]', ''),
    ('ESE-8', '','','', 'Please consult the current undergraduate calendar '
     'for approved electives:  (approved humanities elective, Natural Science '
     'elective)'),

    ('EVSE-3', 'ENEV', '223', '001|091', ''),
    ('EVSE-3', 'ENGG', '240', '00[12]|091', ''),
    ('EVSE-3', 'ENEV', '372', '001|091', ''),
    ('EVSE-3', 'CHEM', '140', '001|09[79]', ''),
    ('EVSE-3', 'GEOL', '102', '001|09[2-9]', ''),

    ('EVSE-5', 'ENEV', '261', '00[12]|092|081', ''),
    ('EVSE-5', 'ENEV', '321', '001', ''),
    ('EVSE-5', 'BIOL', '223', '001', ''),
    ('EVSE-5', 'ENGG', '330', '00[12]|093', ''),
    ('EVSE-5', 'ENEV', '442', '001|091', ''),

    ('EVSE-8', 'ENGG', '303', '001|082', ''),
    ('EVSE-8', 'ENEV', '363', '001|09[12]', ''),
    ('EVSE-8', 'ENEV', '383', '001|09[1-3]', ''),
    ('EVSE-8', 'ENEV', '400', '001', ''),
    ('EVSE-8', 'ENEV', '435', '001', ''),
    ('EVSE-8', 'ENPE', '490', '001|091', ''),
    ('EVSE-8', 'ENEV', '408', '001', ''),
    ('EVSE-8', '','','','One approved elective. Please refer to the '
     'undergrad calendar for a list of approved electives.'),

    ('ISE-3', 'ENEL', '280', '001|09[578]|08[12]', ''),
    ('ISE-3', 'MATH', '217', '001', ''),
    ('ISE-3', 'ENGG', '240', '001|091', ''),
    ('ISE-3', 'ENEV', '223', '001|091', ''),
    ('ISE-3', '','','','Natural Science Elective: (Astronomy, '
     'Biology, Chemistry, Geology, & Physics)'),

    ('ISE-5', 'ENEV', '261', '00[12]|09[1-3]|081', ''),
    ('ISE-5', 'ENGG', '330', '00[12]|09[12]', ''),
    ('ISE-5', 'ENIN', '343', '001|091', ''),
    ('ISE-5', 'CHEM', '140', '001|09[679]', ''),
    ('ISE-5', 'ENIN', '331', '001|091', ''),

    ('ISE-8', 'ENGG', '303', '001|08[12]', ''),
    ('ISE-8', 'ENIN', '340', '001|091', ''),
    ('ISE-8', 'ENIN', '349', '001|091', ''),
    ('ISE-8', 'BUS', '250', '003', ''),
    ('ISE-8', 'ENIN', '400', '001|091', ''),
    ('ISE-8', 'ENIN', '444', '001|09[12]', ''),

    ('PSE-3', 'ENEL', '280', '001|09[2358]|08[12]', ''),
    ('PSE-3', 'ENPE', '241', '001|091', ''),
    ('PSE-3', 'ENGG', '240', '002|091', ''),
    ('PSE-3', 'GEOL', '102', '001|09[25689]', ''),
    ('PSE-3', 'MATH', '217', '001', ''),

    ('PSE-5', 'ENEV', '261', '001|09[13]|081', ''),
    ('PSE-5', 'ENGG', '330', '00[12]|09[124]', ''),
    ('PSE-5', 'ENGG', '303', '001|08[12]', ''),
    ('PSE-5', 'CHEM', '140', '001|09[6-9]', ''),
    ('PSE-5', 'ENEV', '223', '001|091', ''),

    ('PSE-8', 'ENPE', '400', '001', ''),
    ('PSE-8', 'ENPE', '410', '001|09[1-4]', ''),
    ('PSE-8', 'ENPE', '440', '001|09[1-4]', ''),
    ('PSE-8', 'ENPE', '450', '001|09[1-4]', ''),
    ('PSE-8', 'ENPE', '460', '001|09[1-4]', ''),
    ('PSE-8', 'ENPE', '470', '001|08[12]', ''),
    ('PSE-8', 'ENPE', '490', '001|091', ''),

    ('SSE-3', 'ENEL', '280', '001|09[58]|08[12]', ''),
    ('SSE-3', 'ENGG', '240', '00[12]|091', ''),
    ('SSE-3', 'MATH', '217', '001', ''),
    ('SSE-3', 'CS', '115', '001|09[1-5]', ''),
    ('SSE-3', 'ENEV', '223', '001|091', ''),

    ('SSE-5', 'CS', '215', '001|09[68]', ''),
    ('SSE-5', 'CS', '340', '001', ''),
    ('SSE-5', 'ENSE', '374', '001|09[1-3]', ''),
    ('SSE-5', 'ENSE', '352', '001|09[1-3]', ''),
    ('SSE-5', 'ENEL', '384', '001|09[1-3]', ''),

    ('SSE-8', 'ENGG', '303', '001|08[12]', ''),
    ('SSE-8', 'ENSE', '400', '001', ''),
    ('SSE-8', 'ENSE', '472', '001', ''),
    ('SSE-8', 'ENSE', '479', '001', ''),
    ('SSE-8', '','','','Please consult the current undergraduate '
     'calendar for approved elective.'),
]

# Format of a reject rule:
#    (subj_code, crse_numb, seq_numb, tag, comment)

reject_list = [
    ('ENGL', '100', '.*', '.*', "Leave out all engl100 sections, for all engineers",),
    ('CHEM', '104', '004', '.*', "Leave out chem104-004, for all engineers",),
    ('CHEM', '104', '.*', 'COMMON-1-B', "COMMON-1-B doesn't take chem104",),
    ('CHEM', '104', '085|098', 'COMMON-1-A', "COMMON-1-A doesn't take chem104-085,098",),

    ('PHYS', '109', '002', 'COMMON-1-A', "COMMON-1-A takes phys109-001/003",),
    ('PHYS', '109', '125|127|135|155', 'COMMON-1-A', "COMMON-1-A doesn't take phys109-125,127,135,155",),

    ('PHYS', '109', '1[235][0-9]', 'COMMON-1-B', "COMMON-1-B takes phys109-141/143",),
    ('PHYS', '109', '00[12]', 'COMMON-1-B', "COMMON-1-B doesn't take phys109-001,002",),

    ('MATH', '110', '[CL]..', 'COMMON-1-.', "COMMON-1-A/B doesn't take math110 sections Cxx or Lxx"),
    ('MATH', '110', '00[124]', 'COMMON-1-A', "COMMON-1-A doesn't take math110-001, 002, 004"),
    ('MATH', '110', '0[124]0', 'COMMON-1-A', "COMMON-1-A doesn't take math110-003 010,020,040"),
    ('MATH', '110', '00[2-4]', 'COMMON-1-B', "COMMON-1-B doesn't take math110-002,003,004"),
    ('MATH', '110', '0[2-4]0', 'COMMON-1-B', "COMMON-1-B doesn't take math110-020,030,040"),

    ('MATH', '122', 'L01', 'COMMON-1-.', "COMMON-1-A/B doesn't take math122-L01",),
    ('MATH', '122', '00[23]', 'COMMON-1-B', "COMMON-1-B takes math122-001",),
    ('MATH', '122', '00[13]', 'COMMON-1-A', "COMMON-1-A takes math122-002",),

    ('ENGG', '123', '002', 'COMMON-1-A', "COMMON-1-A doesn't take engg123-002",),
]

import re


def _match(tag, timeslot, rule):
    '''
    >>> _match( 'SSE-4', ('ENGL', '100', '001'), ('ENGL', '100', '.*', '.*', 'comment'))
    True
    >>> _match( 'SSE-4', ('ENGL', '100', '001'), ('ENGL', '100', '002', '.*', 'comment'))
    False
    >>> _match( 'EVSE-8', ('ENGL', '100', '002'), ('ENGL', '100', '00[13-9]', 'EVSE-8', 'cmt'))
    False
    >>> _match( 'EVSE-8', ('ENGL', '100', '002'), ('ENGL', '100', '00[1-9]', 'EVSE-8', 'cmt'))
    True
    '''
    (subj_code, crse_numb, seq_numb_re, tag_re, comment) = rule
    if re.match(tag_re, tag) and rule[0:2] == timeslot[0:2] \
       and re.match(seq_numb_re, timeslot[2]):
        return True             # will remove if reject rule matches
    return False                # will not remove

def _purge(tag, ts_list, reject_list):
    '''
    >>> _purge('COMMON-1-B', [(u'ENGL', u'100', u'001', u'Lecture', u'0730', u'0820', (u'T', u'W', u'R')), (u'ENGL', u'100', u'005', u'Lecture', u'1630', u'1720', (u'M', u'T', u'R')), (u'ENGL', u'100', u'006', u'Lecture', u'1300', u'1415', (u'T', u'R')), (u'ENGL', u'100', u'010', u'Lecture', u'1430', u'1545', (u'T', u'R')), (u'ENGL', u'100', u'011', u'Lecture', u'1430', u'1545', (u'T', u'R')), (u'ENGL', u'100', u'013', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'ENGL', u'100', u'014', u'Lecture', u'1000', u'1115', (u'T', u'R')), (u'ENGL', u'100', u'015', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'ENGL', u'100', u'018', u'Lecture', u'1800', u'2045', (u'R',)), (u'ENGL', u'100', u'021', u'Lecture', u'0830', u'0945', (u'T', u'R')), (u'ENGL', u'100', u'022', u'Lecture', u'1000', u'1115', (u'R',)), (u'ENGL', u'100', u'022', u'Lecture', u'1000', u'1115', (u'T', u'R')), (u'ENGL', u'100', u'023', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'ENGL', u'100', u'C07', u'Lecture', u'1630', u'1720', (u'M', u'T', u'R')), (u'ENGL', u'100', u'C08', u'Lecture', u'1630', u'1720', (u'M', u'T', u'R')), (u'ENGL', u'100', u'L01', u'Lecture', u'0930', u'1020', (u'M', u'T', u'R')), (u'ENGL', u'100', u'L03', u'Lecture', u'1300', u'1415', (u'T', u'R')), (u'ENGL', u'100', u'L04', u'Lecture', u'1230', u'1320', (u'T', u'R', u'F')), (u'ENGL', u'100', u'L05', u'Lecture', u'1000', u'1115', (u'T', u'R')), (u'ENGL', u'100', u'L07', u'Lecture', u'1130', u'1220', (u'M', u'T', u'R')), (u'ENGL', u'100', u'L09', u'Lecture', u'1000', u'1115', (u'T', u'R')), (u'ENGL', u'100', u'L10', u'Lecture', u'1430', u'1545', (u'T', u'R')), (u'ENGL', u'100', u'S01', u'Lecture', u'1430', u'1545', (u'T', u'R')), (u'ENGL', u'100', u'S02', u'Lecture', u'1430', u'1545', (u'T', u'R')), (u'ENGL', u'100', u'S04', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'ENGL', u'100', u'S06', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'MATH', u'122', u'002', u'Lecture', u'1630', u'1720', (u'M', u'T', u'R')), (u'MATH', u'122', u'003', u'Lecture', u'1630', u'1745', (u'T', u'R')), (u'MATH', u'122', u'L01', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'PHYS', u'109', u'121', u'Lab', u'0830', u'1115', (u'R',)), (u'PHYS', u'109', u'123', u'Lab', u'0830', u'1115', (u'R',)), (u'PHYS', u'109', u'143', u'Lab', u'1430', u'1715', (u'R',)), (u'MATH', u'110', u'003', u'Lecture', u'1430', u'1520', (u'T', u'R', u'F')), (u'MATH', u'110', u'004', u'Lecture', u'1430', u'1520', (u'T', u'R', u'F'))], [ ('ENGL', '100', '.*', '.*', "Leave out all engl100 sections, for all engineers",),])
    [(u'MATH', u'122', u'002', u'Lecture', u'1630', u'1720', (u'M', u'T', u'R')), (u'MATH', u'122', u'003', u'Lecture', u'1630', u'1745', (u'T', u'R')), (u'MATH', u'122', u'L01', u'Lecture', u'1130', u'1245', (u'T', u'R')), (u'PHYS', u'109', u'121', u'Lab', u'0830', u'1115', (u'R',)), (u'PHYS', u'109', u'123', u'Lab', u'0830', u'1115', (u'R',)), (u'PHYS', u'109', u'143', u'Lab', u'1430', u'1715', (u'R',)), (u'MATH', u'110', u'003', u'Lecture', u'1430', u'1520', (u'T', u'R', u'F')), (u'MATH', u'110', u'004', u'Lecture', u'1430', u'1520', (u'T', u'R', u'F'))]
    '''
        
    for rule in reject_list:
        ts_list = [ts for ts in ts_list if not _match(tag, ts, rule)]
    return ts_list

def _keep(tag, ts_list, accept_list):
    # >>> _keep('ESE-3', [('ENEL', '280', '092', 'Lab', '0830', '1115',('M')),('ENEL', '280', '081', 'Sem', '0830', '1115',('T')),]
    # for rule in accept_list:
    #     if 
    return ts_list

def apply_constraints(per_day_dict, tag, reject_list):
    '''per_day_dict is a dictionary of lists of timeslots where the keys
    are the weekday codes.  The timeslots lists are not yet sorted by
    increasing start time at this point.  A timeslot list may be
    empty.
    
    {'M': [timeslot1, timeslot2],
     'T': [timeslot1],
     'W': [timeslot1, timeslot2, timeslot3],
     'R': [],
     'F': [timeslot1]}

    '''
    for d in per_day_dict:
        timeslot_list = per_day_dict[d]
        per_day_dict[d] = _purge(tag, timeslot_list, reject_list)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
