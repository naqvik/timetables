''' Unit tests for rd_course_comb.py'''
from rd_course_comb import validate, read_course_comb_sheet

import unittest
import xlwt

def _create_course_comb_sheet(xl_filename='test_course_comb.xls'):
    test_course_comb = xlwt.Workbook()
    sheet = test_course_comb.add_sheet('Fall 201430')

    course_section_ids = (

        ('ES-PSE-3', 'ENGG240'),
        ('ES-PSE-3', 'ENEL280'), ('ES-PSE-3', 'ENPE241'),
        ('ES-PSE-3', 'GEOL102'), ('ES-PSE-3', 'MATH217'),
        ('', ''),
        ('ES-SSE-3', 'ENEV 223'),
        ('ES-SSE-3', 'ENEL280'), ('ES-SSE-3', 'CS  115'),
        ('ES-SSE-3', 'ENGG 240'), ('ES-SSE-3', 'MATH217'),
        ('', ''),
        ('ES-COMMON-1-A', 'CHEM104'),
        ('ES-COMMON-1-A', 'ENGG123'), ('ES-COMMON-1-A', 'MATH122'),
        ('ES-COMMON-1-A', 'PHYS109'), ('ES-COMMON-1-A', 'MATH110'),
    )

    start_row = 7
    (col_A, col_H) = (0,7)

    i = 0
    for (course_comb_id, course_code) in course_section_ids:
        sheet.write(start_row + i, col_A, course_comb_id)
        sheet.write(start_row + i, col_H, course_code)
        i += 1
    test_course_comb.save(xl_filename)

if __name__ == "__main__":

    class Test_validate(unittest.TestCase):
        def test_null_record(self):
            result = validate('', '')
            self.assertEqual(result, ('','','','',''))
        def test_no_ext(self):
            result = validate('ES-PSE-3', 'ABCD123')
            self.assertEqual(result, ('PSE-3', '3', '', 'ABCD', '123'))
        def test_ext(self):
            result = validate('ESBASC-SSE-5-A', 'XYZ 105AA')
            self.assertEqual(result, ('SSE-5-A', '5', 'A', 'XYZ', '105AA'))
        def test_bad_tag(self):
            with self.assertRaises(Exception):
                result = validate('ES-SSS-5', 'MATH 110')
        def test_bad_course_code(self):
            with self.assertRaises(Exception):
                result = validate('ES-ESE-3', 'MATH')

    class Test_read_course_comb_sheet(unittest.TestCase):
        def test_reading_spreadsheet(self):
            _create_course_comb_sheet('test_course_comb.xls')
            result = read_course_comb_sheet('2014', 'test_course_comb.xls')
            self.assertEqual(
                result['PSE-3'],
                [ ('ENGG','240'), ('ENEL','280'), ('ENPE','241'),
                  ('GEOL','102'), ('MATH','217'),]
            )

            self.assertEqual(
                result['COMMON-1-A'],
                [('CHEM','104'), ('ENGG','123'), ('MATH','122'),
                ('PHYS','109'), ('MATH','110'),]
            )

            self.assertEqual(
                result['SSE-3'],
                [('ENEV','223'), ('ENEL','280'), ('CS','115'),
                 ('ENGG','240'), ('MATH','217'),]
            )


    unittest.main()
