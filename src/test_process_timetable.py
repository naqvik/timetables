import os
import unittest
from  process_timetable import process, ensure_good_output_filepath

class Test_ensure_good_output_filepath(unittest.TestCase):
    def test_all_components_present(self):
        input = '/h/jim/timetables/out/201430-tt.xls'
        expected = input
        result = ensure_good_output_filepath('201430', input)
        self.assertEqual(result, expected)
    def test_missing_ext(self):
        input = 'blarg'
        expected = './blarg.xls'
        result = ensure_good_output_filepath('201430', input)
        self.assertEqual(result, expected)
    def test_trailing_dir_missing_leafname(self):
        dirpath = 'tempdir'
        try:
            os.mkdir(dirpath)
            input = dirpath
            expected = dirpath + '/201430-timetables.xls'
            result = ensure_good_output_filepath('201430', input)
            try:
                self.assertEqual(result, expected)
            finally:
                os.rmdir(dirpath)
        except OSError as e:
            raise e

    def test_dot(self):
        input= '.'
        expected = './201430-timetables.xls'
        result = ensure_good_output_filepath('201430', input)
        self.assertEqual(result, expected)
        
if __name__ == '__main__':
    unittest.main()
    # term = '201430'
    # constraint_file = '/home/naqvik/proj/engg-admin/timetables/spreadsheets/test-course-comb.xlsx'
    # schedule_file = '/home/naqvik/proj/engg-admin/timetables/spreadsheets/test-schedule.xlsx'
    # # FIXME: should not require a trailing slash
    # output_dir = '../test_output/'
    
    # process(term, schedule_file, constraint_file, output_dir)
