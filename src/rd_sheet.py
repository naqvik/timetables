# Read excel spreadsheet with timetable info

import xlrd
import sys
# sys.path.insert(1, "../../../course-flow/src")
# from course_structure import process_all_programs
#from wt_sheet import create_timetables

def sort_timeslots(timeslot_list):
    '''Given a set of timeslots, corresponding to a particular weekday,
       return a sorted copy of the list.

    Each timeslot has the format:
       SUBJ_CODE CRSE_NUMB SUBJ_CODE STVSCHD_DESC BEGIN_TIME END_TIME list_of_days

    '''
    def compare(x,y):
        x_begin_time = x[4]
        y_begin_time = y[4]
        result = 0
        if x_begin_time < y_begin_time:
            result = -1
        elif x_begin_time == y_begin_time:
            result = 0
        else:
            result = 1
        return result

    # return a sorted copy of the input
    temp = timeslot_list[:]
    temp.sort(compare)
    return temp

def read_schedule_sheet(filename = "../spreadsheets/201430 - all classes.xlsx"):
    '''Given an excel spreadsheet filename, return a handle to sheet 0 of
    that worksheet.

    '''
    try:
        workbook = xlrd.open_workbook(filename)
        engg_sheet = workbook.sheet_by_index(0)
    except Exception as e:
        raise Exception(
            'An error occurred processing the schedule sheet:\n  %s\n'
            % filename +
            'Either the sheet does not exist, or it is malformed.')
    return engg_sheet
    
def create_timeslot(row_dict):
    '''Return a new timeslot (describes a single chunk of time on a single
    day allocated to a certain class component like a lecture, lab, or
    seminar).

    The structure of a timeslot is a tuple with
    ( subj_code, crse_numb, seq_numb, stvschd_desc, begin_time, end_time,
       ( day_codes ) )

    Where the day_codes tuple has at least one of the following
    single-character day codes: 'M', 'T', 'W', 'R', 'F', in that order.

    Useful fieldsnames are: SUBJ_CODE, CRSE_NUMB, SEQ_NUMB, STVSCHD_DESC,
    BEGIN_TIME, END_TIME, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY.

    '''
    def create_weekdays_tuple(row_dict):
        day_names = [ 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY' ]
        day_codes = [ row_dict[d] for d in day_names ]
        result = [d for d in day_codes if d != ''] # omit empty strings
        return tuple(result)

    result = (
        row_dict['SUBJ_CODE'], 
        row_dict['CRSE_NUMB'],
        row_dict['SEQ_NUMB'],
        row_dict['STVSCHD_DESC'],
        row_dict['BEGIN_TIME'],
        row_dict['END_TIME'], 
        create_weekdays_tuple(row_dict)
    )
    return result
        

def matching_course_sections(sheet, course_section_id):
    '''Return a list of all CourseSections which match the provided
    course section ID (a 2-tuple of subj_code and crse_numb).

    For example for 201430, (ENEL, 280), should return a list of CourseSections:
    [
    (ENEL, 280, 001, Lecture, 1530, 1620, (M, W, F)),
    (ENEL, 280, 081, Seminar, 0830, 1115, (T, )),
    (ENEL, 280, 082, Seminar, 1430, 1715, (T, )),			
    (ENEL, 280, 091, Lab,     1430, 1715, (T, )),
    (ENEL, 280, 092, Lab,     0830, 1115, (M, )),
    (ENEL, 280, 093, Lab,     0830, 1115, (F, )),
    (ENEL, 280, 094, Lab,     1430, 1715, (R, )),
    (ENEL, 280, 095, Lab,     1430, 1715, (T, )),
    (ENEL, 280, 096, Lab,     1130, 1415, (F, )),
    (ENEL, 280, 097, Lab,     1130, 1415, (W, )),
    (ENEL, 280, 098, Lab,     1430, 1715, (R, )),
    ]

    '''
    fields = sheet.row_values(0)
    result = []
    for r in xrange(1, sheet.nrows):
        values = sheet.row_values(r)
        row_dict = dict(zip(fields, values))

        subj_code, crse_numb = course_section_id
        if row_dict['SUBJ_CODE'] == subj_code and \
           row_dict['CRSE_NUMB'] == crse_numb:
            result.append( create_timeslot(row_dict) )
    return result

def group_course_sections_by_day(course_section_list,
                                 course_sections_by_day = None):
    '''Given a list of CourseSections, create (or append to) a dictionary
    where the keys are day codes (MTWRF), and the values are
    (unsorted) CourseSection lists.

    '''
    if course_sections_by_day == None:
        course_sections_by_day = create_empty_per_day_dict()
    for crs in course_section_list:
        days = crs[-1]
        for day in days:
            course_sections_by_day[day].append(crs)
    return course_sections_by_day

def get_one_semester(pgms, pgm, semester):
    '''Return the classes for a given program and semester, from the pgms
    dictionary, which itself is implicitly for a particular year.
    '''
    courses = pgms[pgm][semester]
    return courses

def create_empty_per_day_dict():
    return dict( [('M', []), ('T', []), ('W', []), ('R', []), ('F', [])] )
    
def create_per_day_dict(tag, sheet, courses):
    """Given a tag (eg. "COMMON-1-A'), the open schedule worksheet file
    handle, and a list of courses, eg. ["CHEM 104", "ENGG 123", "MATH
    122", "PHYS 109", "MATH 110"], return a dictionary containing
    unsorted course sections, organized by weekday.


    Example: On input the following CourseSection objects might be
    read from the sheet file (along with MANY others)

    ("CHEM", "104", "001", "Lec", "1530", "1645", ("W", "F",), "Brian Sterenberg"),
    ("CHEM", "104", "002", "Lec", "1130", "1220", ("T", "R", "F",), "Stephen Cheng"),
    ("CHEM", "104", "004", "Lec", "1230", "1320", ("M", "W", "F",), "Olena Lukoyanova"),
    ("CHEM", "104", "085", "Lab", "0830", "1120", ("R", ), "Mark Tymchak"),
    ("CHEM", "104", "086", "Lab", "0830", "1115", ("M", ), "Mark Tymchak"),
    ("CHEM", "104", "087", "Lab", "1430", "1715", ("M", ), "Mark Tymchak"),
    ("CHEM", "104", "088", "Lab", "0830", "1115", ("W", ), "Mark Tymchak"),
    ("CHEM", "104", "089", "Lab", "1430", "1715", ("R", ), "Mark Tymchak"),
    ("CHEM", "104", "090", "Lab", "0830", "1115", ("R", ), "Mark Tymchak"),
    ("CHEM", "104", "091", "Lab", "1430", "1715", ("R", ), "Mark Tymchak"),
    ("CHEM", "104", "092", "Lab", "0830", "1115", ("F", ), "Mark Tymchak"),
    ("CHEM", "104", "093", "Lab", "0830", "1115", ("T", ), "Mark Tymchak"),
    ("CHEM", "104", "094", "Lab", "0830", "1115", ("W", ), "Mark Tymchak"),
    ("CHEM", "104", "095", "Lab", "1430", "1715", ("W", ), "Mark Tymchak"),
    ("CHEM", "104", "096", "Lab", "1430", "1715", ("T", ), "Mark Tymchak"),
    ("CHEM", "104", "097", "Lab", "1430", "1715", ("W", ), "Mark Tymchak"),
    ("CHEM", "104", "099", "Lab", "0830", "1115", ("F", ), "Mark Tymchak)),





    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),
    ("ENGG", "123", "002", "Lec", "1300", "1415", ("T", "R",), "David de Montigny"),
    ("ENGG", "123", "091", "Lab", "0830", "1115", ("T", "R",), "David de Montigny"),
    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),
    ("ENGG", "123", "002", "Lec", "1300", "1415", ("T", "R",), "David de Montigny"),
    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),

    ("MATH", "122", "001", "Lec", "1030", "1120", ("M", "W", "F"), "Christos Chorianopoulos"),
    ("MATH", "122", "002", "Lec", "1630", "1720", ("M", "T", "R"), "Chun-Hua Guo"),
    ("MATH", "122", "003", "Lec", "1630", "1745", ("T", "R"), "Brian Ketelboeter"),
    ("MATH", "122", "L01", "Lec", "1130", "1245", ("T", "R"), "Fotini Labropulu")


    ("PHYS", "109", "001", "Lec", "0830", "0945", ("M", "W"), "Pierre-Philippe Ouimet"),
    ("PHYS", "109", "003", "Lec", "1730", "1820", ("M", "W", "F"), "Olusola Fasunwon"),
    ("PHYS", "109", "121", "Lab", "0830", "1115", ("R", ), ""),
    ("PHYS", "109", "123", "Lab", "0830", "1115", ("R", ), ""),
    ("PHYS", "109", "125", "Lab", "0830", "1120", ("T", ), ""),
    ("PHYS", "109", "127", "Lab", "0830", "1120", ("T", ). ""),
    ("PHYS", "109", "131", "Lab", "0830", "1115", ("F", ), ""),
    ("PHYS", "109", "133", "Lab", "1430", "1715", ("W", ), ""),
    ("PHYS", "109", "135", "Lab", "0830", "1120", ("W", ), ""),
    ("PHYS", "109", "141", "Lab", "1430", "1715", ("W", ), ""),
    ("PHYS", "109", "143", "Lab", "1430", "1715", ("R", ), ""),
    ("PHYS", "109", "151", "Lab", "1430", "1715", ("T", ), ""),
    ("PHYS", "109", "153", "Lab", "1430", "1715", ("F", ), ""),
    ("PHYS", "109", "155", "Lab", "1430", "1720", ("F", ), ""),

    The output would be a dictionary with keys M, T, W, R, F and
    values being lists of CourseSections that occur on that day.

    Example:
    {
    "M": [
    ("CHEM", "104", "004", "Lec", "1230", "1320", ("M", "W", "F",), "Olena Lukoyanova"),
    ("CHEM", "104", "086", "Lab", "0830", "1115", ("M", ), "Mark Tymchak"),
    ("CHEM", "104", "087", "Lab", "1430", "1715", ("M", ), "Mark Tymchak"),
    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),
    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),
    ("ENGG", "123", "001", "Lec", "1330", "1420", ("M", "W", "F",), "David de Montigny"),

    ("MATH", "122", "001", "Lec", "1030", "1120", ("M", "W", "F"), "Christos Chorianopoulos"),
    ("MATH", "122", "002", "Lec", "1630", "1720", ("M", "T", "R"), "Chun-Hua Guo"),
    ("PHYS", "109", "001", "Lec", "0830", "0945", ("M", "W"), "Pierre-Philippe Ouimet"),
    ("PHYS", "109", "003", "Lec", "1730", "1820", ("M", "W", "F"), "Olusola Fasunwon"),

    ],
    "T": [
    ("CHEM", "104", "002", "Lec", "1130", "1220", ("T", "R", "F",), "Stephen Cheng"),
    ("CHEM", "104", "093", "Lab", "0830", "1115", ("T", ), "Mark Tymchak"),
    ("CHEM", "104", "096", "Lab", "1430", "1715", ("T", ), "Mark Tymchak"),
    ("ENGG", "123", "002", "Lec", "1300", "1415", ("T", "R",), "David de Montigny"),
    ("ENGG", "123", "091", "Lab", "0830", "1115", ("T", "R",), "David de Montigny"),
    ("ENGG", "123", "002", "Lec", "1300", "1415", ("T", "R",), "David de Montigny"),
    ("MATH", "122", "003", "Lec", "1630", "1745", ("T", "R"), "Brian Ketelboeter"),
    ("MATH", "122", "L01", "Lec", "1130", "1245", ("T", "R"), "Fotini Labropulu")
    ("PHYS", "109", "125", "Lab", "0830", "1120", ("T", ), ""),
    ("PHYS", "109", "127", "Lab", "0830", "1120", ("T", ). ""),
    ("PHYS", "109", "151", "Lab", "1430", "1715", ("T", ), ""),
    ],
    }

    and so on for W, R, and F.

    """
    course_sections_by_day = None
    for course_section_id in courses:
        course_sections = matching_course_sections(sheet, course_section_id)
        course_sections_by_day = group_course_sections_by_day(
            course_sections, course_sections_by_day)
    return course_sections_by_day
        
def sort_all_days(per_day_dict):
    '''Update the per-day dictionary so each day's timeslots are sorted by
    increasing start time.  The argument is modified.

    '''
    for d in ('M', 'T', 'W', 'R', 'F'):
        day_schedule = sort_timeslots (per_day_dict[d])
        per_day_dict[d] = day_schedule



# if __name__ == "__main__":
#     def dump_pgm_semester(sheet, courses):
#         print courses

#         per_day_dict = create_per_day_dict(sheet, courses)
#         sort_all_days(per_day_dict)
#         for d in ['M', 'T', 'W', 'R', 'F']:
#             print d
#             for slot in per_day_dict[d]:
#                 print "%s %s %s\t%.3s %s-%s" % slot[0:-1]

#     all_pgms = get_academic_programs(year=2014)
#     sheet = read_schedule_sheet("../spreadsheets/201430 - all classes.xlsx")
    
#     courses = get_one_semester(all_pgms, 'ese', 1)
#     print 'program:', 'ese', ', semester:', 1
#     dump_pgm_semester(sheet, courses)

#     for pgm in ('ese', 'evse', 'ise', 'pse', 'sse'):
#         for semester in (3,5,8):
#             courses = get_one_semester(all_pgms, pgm, semester)
#             print 'program:', pgm, ', semester:', semester
#             dump_pgm_semester(sheet, courses)
