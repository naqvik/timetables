'''Provides a graphical front-end to the timetable generation program.

The tutorials and documents on www.tkdocs.com proved useful in writing
this code.

'''
from Tkinter import *
#from ttk import *
import ttk
import tkFileDialog
import tkMessageBox
from version import VERSION

import process_timetable

class Timetable_app(object):
    '''Responsible for the GUI front-end to the timetable generation program.
    '''
    def __init__(self,
            term='201430',
            schedules='../spreadsheets/201430 - all classes.xlsx',
            constraints='../spreadsheets/ES Course Comb fixed 201430.xlsx'):

        self.term = term
        self.schedules = schedules
        self.constraints = constraints
        self.timetables_dir = '.'

        self.root = Tk()
        self.root.title('Timetable Generator  ver: ' + VERSION)

        mainframe = ttk.Frame(self.root, padding="10 15 10 15")
        mainframe.grid(row=0, column=0)
        #self.root.columnconfigure(1, weight=1) # does not work?

        # term
        self.term_str = StringVar()
        self.term_str.set(term)
        ttk.Label(mainframe, text='Term:').grid(
            row=0, column=0, sticky=E)
        ttk.Entry(
            mainframe, width=7, textvariable=self.term_str).grid(
                row=0, column=1, sticky=(W),pady=15)

        # schedules
        self.schedules_str = StringVar()
        self.schedules_str.set(schedules)

        ttk.Label(mainframe, text='Schedules:').grid(
            row=1, column=0, sticky=E)
        ttk.Entry(
            mainframe, width=45,
            textvariable=self.schedules_str).grid(
                row=1, column=1, sticky=(W, E), pady=15)
        ttk.Button(mainframe, text='Pick',
                   command=self.pick_schedules).grid(row=1, column=2)

        # constraints
        self.constraints_str = StringVar()
        self.constraints_str.set(constraints)

        ttk.Label(mainframe, text='Constraints:').grid(
            row=2, column=0, sticky=E)
        ttk.Entry(
            mainframe, width=45, textvariable=self.constraints_str).grid(
                row=2, column=1, sticky=(W,E))
        ttk.Button(mainframe, text='Pick',
                   command=self.pick_constraints).grid(row=2, column=2)

        # output file
        self.output_str = StringVar()
        self.output_str.set('./201430-timetables.xls')
        ttk.Label(mainframe, text='Output:').grid(
            row=3, column=0, sticky=E)
        ttk.Entry(mainframe,
                  textvariable=self.output_str).grid(
                      row=3, column=1, sticky=(W,E), pady=15)
        ttk.Button(mainframe, text='Pick',
                   command=self.pick_output).grid(row=3, column=2)

        # control buttons
        ttk.Button(mainframe, text='Create',
                   command=self.create).grid(row=4, column=1)
        ttk.Button(mainframe, text='Quit',
                   command=self.root.quit).grid(row=4, column=2)

        self.root.mainloop()

    def pick_schedules(self, *args):
        print 'pick_schedules'
        schedules = tkFileDialog.askopenfilename(title='Choose Schedules Sheet')
        if schedules:
            self.schedules_str.set(schedules)
        print 'result:', self.schedules_str.get()

    def pick_constraints(self, *args):
        print 'pick_constraints'
        constraints = tkFileDialog.askopenfilename(title='Choose Constraints Sheet')
        if constraints:
            self.constraints_str.set(constraints)
        print 'result:', self.constraints_str.get()

    def pick_output(self, *args):
        print 'pick_output'
        output = tkFileDialog.asksaveasfile(title='Choose output file')
        if output:
            self.output_str.set(output)
        print 'result:', self.output_str.get()

    def chooseTerm(self):
        pass
    def create(self):
        print 'create'
        result = tkMessageBox.askokcancel(
            message='Are you sure you want to create the file: %s?' % \
            self.output_str.get(),
            icon='question')
        if result == True:
            try:
                process_timetable.process(
                    self.term_str.get(), self.schedules_str.get(),
                    self.constraints_str.get(), self.output_str.get()
                )
            except Exception as e:
                print e
                tkMessageBox.showinfo("Error", message=e)

            tkMessageBox.showinfo(message="Press OK to quit.")
            self.root.quit()
            
        else:
            print 'cancelled'

app = Timetable_app()
