'''Main routine, invokable from commandline, or from gui.  Produces
all timetables for all programs, currently tested and working for term 201430.

'''
import sys
import re
import os.path
from rd_sheet import read_schedule_sheet
#from rd_sheet import get_academic_programs
from rd_sheet import get_one_semester
from rd_sheet import create_per_day_dict, sort_all_days
from wt_sheet import create_timetables
from reject_accept import reject_list
from reject_accept import apply_constraints
import rd_course_comb

def process(term, course_schedule_file, course_comb_file, output_path):
    """For the given term, read each TAG, and its associated SUBJ_CODE
    and CRSE_NUMB.

    From the schedules sheet, select matching CourseSections.  Group
    these by day, then sort each group by begin time.

    Then generate the timetables as excel files, one for each tag.
    """
    print 'writing to ', output_path
    if re.match('\d{4}[123]0', term) is None:
        raise Exception("Malformed term:\n  " + term + 
                        "\nThe 'term' must have the form YYYYSS,  e.g. 201430")

    # For the given term, get a course section ID list for each tag.
    # That is, the academic program for each tag.
    academic_progs_by_tag = rd_course_comb.read_course_comb_sheet(
        term[0:4], filename=course_comb_file)

    #print academic_progs_by_tag

    # Scan through all schedules, creating a timetable for each tag.
    schedule_sheet = read_schedule_sheet(course_schedule_file)
    all_timetables = []
    for tag in sorted(academic_progs_by_tag):
        per_day_dict = create_per_day_dict(tag, schedule_sheet,
                                           academic_progs_by_tag[tag])
        apply_constraints(per_day_dict, tag, reject_list)
        sort_all_days(per_day_dict)
        all_timetables.append( (tag, per_day_dict.copy()) )

    # FIXME: output file should not be constructed by mere string
    # concatenation, it's not portable
    output_path = ensure_good_output_filepath(term, output_path)
    create_timetables(term, all_timetables, output_path)

def ensure_good_output_filepath(term, output_path):
    if os.path.isdir(output_path):
        # Don't have to check for duplicate slashes. Join
        # will remove duplicates.
        output_path += '/' 
    (output_dir, output_leafname) = os.path.split(output_path)
    (output_basename, output_ext) = os.path.splitext(output_leafname)
    output_dir = output_dir or '.'
    output_basename = output_basename or term+'-timetables'
    output_ext = output_ext or '.xls'
    return os.path.join(output_dir, output_basename + output_ext)

# FIXME: maybe should not depend on slash being the directory separator
if __name__ == "__main__":
    process(term='201430',
            course_schedule_file="../spreadsheets/201430 - all classes.xlsx",
            course_comb_file="../spreadsheets/ES Course Comb fixed 201430.xlsx",
            output_path=ensure_good_output_filepath('201430', '.'))
