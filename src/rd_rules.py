'''Read rules spreadsheet, produce a TimeTables object containing all
TimeTables, 

Each Timetable (identified by a tag) is initialized with sufficient
information to run a whitelist filter over the schedules spreadsheet
and pick out only those courses, lecture sections, lab sections, and
seminar sections relevant to that particular tag.

'''
import re
import xlrd

# this is basically the schema for the rules spreadsheet, and should
# be a global constant
FIELD_NAMES = ('TERM_CODE', 'TAG', 'SUBJ_CODE', 'CRSE_NUMB', 'LECTURES',
               'LABS', 'SEMINARS', 'COMMENT' )

class CourseDisplayRule(object):
    """Represents the way a course offering should be displayed in a
    timetable for a particular tag.  An object of this type captures
    all the information in a single row of the rules spreadsheet.

    For example, the course ENEL 280 is taken by ESE in semester 3,
    ISE in semester 3, PSE in semester 3, etc.  The associated CourseDisplayRule will have 
    the following section numbers to be displayed
    
    ESE-3 : 001, 095, 098, 081, 082
    ISE-3 : 001, 095, 097, 098, 081, 082
    PSE-3 : 001, 095, 092, 093, 095, 098, 081, 082

    A course display rule will have 
      a term_code
      a Tag
      a subj_code
      a set of sequence numbers
      an optional comment

    A the sequence number set will contain sequence numbers from:
      one or more lecture sections,
      zero or more labs, and
      zero or more seminars, as well as

    parms is expected to be a tuple with string members (tag,
    subj_code, crse_numb, lectures, labs, seminars, comment)

    """
    def __init__(self, parms, seq_numbers=set()):
        # print parms
        # print seq_numbers
        assert len(FIELD_NAMES) == len(parms)
        self.row_dict = dict( zip(FIELD_NAMES, parms) )
        self.field_names = FIELD_NAMES
        self.parms = parms
        self.seq_numbers = seq_numbers

    def __eq__(self, other):
        return self.parms == other.parms


class TimeTable(object):
    """Represents all the course offerings pertaining to a single program,
    semester, and partition.  The engg faculty partitions programs and
    semesters for load balancing purposes, for example, at this time
    we take the COMMON first year program, which includes semesters 1
    and 2, and divide each into part A and B, giving a total of 4
    separate timetables, with tags COMMON-[12]-[AB],

    """
    def __init__(self):
        self.course_display_rules = []
        self.comment = ''

    def add_course(self, course_display_rule):
        """Add a course display rule to this timetable"""
        self.course_display_rules.append(course_display_rule)
    def add_comment(self, comment):
        """Add a global comment for this timetable"""
        self.comment = comment
    def get_courses(self):
        """Return list of course display rules."""
        return self.course_display_rules

class TimeTables(object):
    """Represents a set of timetables, each one identified by a "tag", a
    program-sem-part triplet (but the last '-part' is optional).

    """
    def __init__(self):
        """A tag->Timetable mapping class."""
        self._timetables = {}

    def __len__(self):
        return len(self._timetables)

    def timetable(self, tag):
        """Add new tag, or retrieve existing tag."""
        if tag not in self._timetables:
            self._timetables[tag] = TimeTable()
        return self._timetables[tag]

class ValidRuleException(Exception):
    """An invalid rule was encountered."""
    pass

def valid_tag(tag):
    """Return nothing if tag is valid, else raise exception.
    """
    tag_re = re.compile(
        r"(ESE|EVSE|ISE|PSE|SSE|COMMON)-([1-9])(-([A-Z]))?")

    tag_m = re.match(tag_re, tag)
    if (tag_m == None):
        raise(ValidRuleException(
            #'Error reading TAG in rules sheet in row %d:' % (row+1,) +
            "Error reading TAG in rules sheet.  " +
            "Malformed tag encountered: '%s'\n" % tag)
          )
def valid_subj_code(vals, subj_code):
    """Return nothing if subject code is valid, else raise exception.  A
    valid subject code must match a specific pattern, or be blank (in
    which case the next 4 fields must also be blank).

    """
    
    if subj_code == '':
        if any(vals[3:-1]):
            raise(ValidRuleException(
                'Error: A rule with a blank SUBJ_CODE ' +
                'requires CRSE_NUMB, LECTURES, LABS, SEMINARS fields ' +
                'to also be blank.')
              )
        else:
            return  # accept entries with all but COMMENT blank
    subj_code_re = r'^([A-Z]{2,4})$'
    subj_code_m = re.match(subj_code_re, subj_code)
    if subj_code_m == None:
        raise(ValidRuleException(
            'Error reading SUBJ_CODE in rules sheet.  ' +
            "Malformed subject code encountered: '%s'\n" % subj_code)
        )

def valid_crse_numb(crse_numb):
    """Return nothing if course number is valid, else raise exception.
    """
    crse_numb_re = r'([0-9]{3}([A-Z]{2})?)'
    crse_numb_m = re.match(crse_numb_re, crse_numb)
    if crse_numb_m == None:
        raise(ValidRuleException(
            "Error reading CRSE_NUMB in rules sheet.  " +
            "Malformed course number encountered: '%s'\n" % crse_numb)
        )

def valid_seq_numb(seq_numb):
    """Ensure this field is a comma-separated series of ranges.  If so,
    return the equivalent expanded set of integers, as a set, otherwise
    throw a ValidRuleException.

    """
    def expand_range(range_):
        """It's a range, expand into a list."""
        m = re.match(r"^(\d{3})-(\d{3})$", range_)
        if m:
            low = int(m.group(1))
            high = int(m.group(2)) + 1
            return range(low, high)
        raise(ValidRuleException(
            "Error interpreting sequence number or range: '%s'" % range_
        ))
    ranges = seq_numb.split(",") # split on commas
    elements = []
    for elem in ranges:
        if not elem:
            continue
        elif re.match(r"^(\d{3})$", elem):
            elements.append( int(elem) )
        else:
            elements += expand_range(elem)

    return set(elements)

def valid_rule(term, vals):
    """Ensure this rule is valid, converting floats to strings if
    necessary.  Return a 2-tuple (Boolean, set).  The boolean
    indicates whether this record should be retained or skipped.  The
    set contains all the sequence numbers (lecture sections, lab
    sections, seminar sections) which should be retained when scanning
    through the main schedule.

    """
    # make sure there are no floats, which should be unicode/str strings
    for i, v in enumerate(vals):
        if not isinstance(v, unicode) and not isinstance(v, str):
            vals[i] = unicode(int(v))


    assert len(FIELD_NAMES) == len(vals)
    field = dict( zip(FIELD_NAMES, vals) )

    if field['TERM_CODE'] != term:
        return (False, set()) # silently ignore rules for non-matching terms

    valid_tag(field['TAG'])
    valid_subj_code(vals, field['SUBJ_CODE'])
    if field['SUBJ_CODE'] == '':
        return (True, set())  # accept entries with all but COMMENT blank

    valid_crse_numb(field['CRSE_NUMB'])

    seq_number_set = valid_seq_numb(field['LECTURES']).union(
        valid_seq_numb(field['LABS'])).union(
            valid_seq_numb(field['SEMINARS']))

    return (True, seq_number_set)


def read_rules(term, filename):
    """Open the rules excel spreadsheet and return a TimeTables object,
    which contains a dictionary of TimeTables, keyed by the tag.

    """
    try:
        workbook = xlrd.open_workbook(filename)

        sheet = workbook.sheet_by_index(0)  # get the first tab
        start_row = 2                       # skip rows of header info
        # FIXME: should maybe have no header at all?

        # validate field names
        field_names = sheet.row_values(start_row)
        if tuple(field_names) != FIELD_NAMES:
            raise Exception(
                "Error: invalid rules sheet.  Expected %r.  Got %r"
                % (FIELD_NAMES, field_names))

        timetables = TimeTables()
        # # add a timetable for each tag
        # for t in FIELD_NAMES:
        #     timetables.timetable(t)

        for row in range(start_row+1, sheet.nrows):
            #print 'processing row:', row+1
            values = sheet.row_values(row)
            (is_valid, seq_number_set) = valid_rule(
                term, values)
            if is_valid:
                tag = values[1]
                if not seq_number_set:  # must be a TimeTable global comment
                    timetables.timetable(tag).add_comment(values[-1])
                else:
                    course_offering = CourseDisplayRule(
                        values, seq_number_set)
                    timetables.timetable(tag).add_course(course_offering)

        return timetables
    except ValidRuleException as vre:
        msg = 'Row %d: ' % (row+1) + vre.message
        raise ValidRuleException(msg)
    except IOError as ex:
        raise IOError(
            "'An error occurred processing the rules sheet: '%s'\nEither "
            "the sheet does not exist, or it is malformed." % filename)

    except Exception:
        raise
