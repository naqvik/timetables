'''Read course comb spreadsheet, which has 5 pieces of information
    spread over 2 columns and 1 filename:

  - column A: has format
     <degree-or-faculty>-<program>-<sem#>{-<subgroup>}
    where the last part is optional.

    The degree or faculty should be ignored. The program is one of the
    set ESE, EVSE, ISE, PSE, or SSE.  Surprisingly, the program may
    also be the text 'COMMON', 'CATCH', 'ENHS', or 'JIDP'.  It seems
    that of these only 'COMMON' is relevant to this project: it is
    used because engineering has a common first year so implicitly the
    following sem# must be 1 or 2.


    The sem# is a number from 1-9.

    The subgroup is (maybe?) a letter A, B, etc.

  - column H is the course being offered to that
    program/semester/subgroup.  This has a form such as: 'ENIN233',
    'ENIN 233', 'ENIN 233', 'CS 115', etc.  In other words there are
    zero, one, or two spaces separating the subject code from the
    course number.

  - the year and semester code is taken from the filename and has the
    form YYYYSS, where YYYY is a 4-digit year and SS is a semester
    code like 10, 20, or 30.  There are other semester codes, but it
    appears they need not be handled for this project.

  - a record may also contain blanks in column A and H, in which case
    it is silently ignored.

'''
import re
import xlrd

def read_course_comb_sheet(year, filename="../spreadsheets/ES Course Comb fixed 201430.xlsx"):
    '''Open the given excel spreadsheet and return a dictionary of
    CourseSectionID lists grouped by tag.  A course section ID is a
    2-tuple like ('MATH', '122').

    The input sheet has to have columns A and H, where col A contains
    a course_comb_id with a prefix of either "ES-" or "ESBASC-".  Col
    H contains a string containing the course name SUBJ_CODE and the
    course number CRSE_NUMB, separated by zero or more spaces.

    '''
    try:
        workbook = xlrd.open_workbook(filename)

        sheet = workbook.sheet_by_index(0)  # get the first tab
        start_row = 7                       # skip rows of header info
        (col_A, col_H) = (0,7)

        result = {}
        for r in xrange(start_row, sheet.nrows):
            (course_comb_id, course_code) = (
                sheet.cell(r,col_A).value, sheet.cell(r, col_H).value )
            (tag, sem, part, subj_code, crse_numb) = validate(
                course_comb_id, course_code)
            if tag == '':       # skip blank records
                continue

            if result.has_key(tag):
                result[tag].append( (subj_code, crse_numb) )
            else:
                result[tag] = [(subj_code, crse_numb)]

        return result
    except Exception as e:
        raise
        # raise Exception('An error occurred processing the constraints sheet:\n  %s\n'
        #       'Either the sheet does not exist, or it is malformed.' % filename)

def validate(course_comb_id, course_code):
    '''Given a TAG with a prefix (like "ES-COMMON-1-A") and a
    coursename-coursenumber pair with a space, like "MATH 122", return
    a 5 tuple of tag, sem, part, subj_code, crse_numb.  

    In this case it would be ('COMMON-1-A', '1', 'A', 'MATH', '122').

    '''
    # deal with null records by returning 5-tuple of empty strings
    if course_comb_id == '' and course_code == '':
        return tuple( ['','','','',''] )

    # match course_comb_id's like 'ES-PSE-3', 'ES-SSE-8',
    # 'ES-COMMON-1-A', 'ESBASC-ESE-8', 'ESBASC-ISE-8', etc.
    course_comb_id_re = r"[A-Z]{1,}-(ESE|EVSE|ISE|PSE|SSE|COMMON)-([1-9])(-([A-Z]))?"

    # match course name/numbers like 'ABCD 123',  'CS 101AZ', etc.
    course_code_re = r'([A-Z]{2,4}) {0,2}([0-9]{3}([A-Z]{2})?)'


    m1 = re.match(course_comb_id_re, course_comb_id)
    if m1 == None:
        raise(Exception('Unexpected course comb id: ' + course_comb_id))

    (prg, sem, p1, part) = m1.groups()

    if p1 == None:
        tag = "-".join( (prg, sem) )
        part = ''
    else:
        tag = "-".join( (prg, sem, part) )

    m2 = re.match(course_code_re, course_code)
    if m2 == None:
        raise(Exception('Unexpected course code: ' + course_code))
    (subj_code, crse_numb, crse_numb_ext) = m2.groups()

    # if crse_numb_ext != None:
    #     crse_numb += crse_numb_ext
    return (tag, sem, part, subj_code, crse_numb)
